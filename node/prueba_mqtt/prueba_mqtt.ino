#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.

const char* ssid = "------";
const char* password = "------";
const char* mqtt_server = "192.168.100.207";
char* str_movimiento = "movimiento";
char* state = "2";

// Leds
int led_der = 5;
int led_izq = 4;

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  pinMode(led_der,OUTPUT);
  pinMode(led_izq,OUTPUT);
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  String msg = new String(payload);
  msg = msg.trim().split('/');
  
  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    state="1"; //derecha
    digitalWrite(led_der, HIGH);
    digitalWrite(led_izq, LOW);   
  }
 
 if((char)payload[0] == '0'){
    state="0";    
    digitalWrite(led_der, LOW);
    digitalWrite(led_izq, HIGH);
   }
  
 if((char)payload[0] != '0' && (char)payload[0] != '1'){
    state="2";    
    digitalWrite(led_der, LOW);
    digitalWrite(led_izq, LOW);
   }
  
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(str_movimiento, state);
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    ++value;
    Serial.println(msg);
    client.publish(str_movimiento, state);
  }
}
