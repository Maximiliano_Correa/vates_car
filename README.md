# vates_car

Introducción

La planificación del siguiente proyecto surge en base al HackDay organizado por la empresa VATES, que nos brinda la posibilidad de crear proyectos, a la vez de intercambiar conocimientos y experiencias.
En este documento vamos a describir nuestro proyecto basado en la identificación de un problema, con sus fundamentos, su resolución y su alcance.

Definición del Problema
El siguiente proyecto tiene como objetivo presentar una propuesta a los colegios para maximizar el uso de los equipamientos tecnológicos que se brindan desde Nación o Provincia, entre ellos las Raspberry Pi.

Descripción General
La idea sería utilizar Cardboards, con el uso del celular de los alumnos, con las cuales al realizar diferentes movimientos con las cabeza se pueda generar un movimiento a un Auto de Juguete. 
![Flowchart](https://gitlab.com/Maximiliano_Correa/vates_car/blob/master/doc/images/Flowchart.png)
El celular utilizado en la Cardboard tendrá un giróscopo controlado por Android, el cual permitirá enviar señales de movimiento a un servidor montado en una Raspberry Pi a través del protocolo de comunicación mqtt. El broker enviará estas señales a un Node CMU que estará conectado al Auto de juguete. El sistema operativo Android publicará continuamente las señales y el node consumirá las mismas. 



